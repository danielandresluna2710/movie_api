from typing import Optional, List
from pydantic import BaseModel, Field


class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(min_length=1, max_length=255)
    overview: str = Field(min_length=1, max_length=255)
    year: int = Field(le=2023)
    rating:float = Field(ge=1, le=10)
    category:str = Field(min_length=1, max_length=255)

    class Config:
        schema_extra = {
            "example": {
                "id": 1,
                "title": "Mi película",
                "overview": "Descripción de la película",
                "year": 2022,
                "rating": 9.8,
                "category" : "Acción"
            }
        }
    
